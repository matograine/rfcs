# Duniter ecosystem documentations

This repository is aimed to provide documentions and RFCs (Request for Changes) used in the whole Duniter ecosystem.

All documents should be implementation agnostic, as specific documentations should be in a dedicated repository (or implementation repository).

## RFCs

* [Approved RFCs](#approved-rfcs)
* [Under discussion RFCs](#under-discussion-rfcs)
* [Drafts RFCs](#drafts-rfcs)
* [Discontinued RFCs](#discontinued-rfcs)

### Approved RFCs

#### DUniter Blockchain Protocol (DUBP)

* V11: [RFC 0009](rfc/0009_Duniter_Blockchain_Protocol_V11.md)
* V12: [RFC 0010](rfc/0010_Duniter_Blockchain_Protocol_V12.md)

#### DUniter Network Protocol (DUNP)

* V1: [RFC 0004](rfc/0004_ws2p_v1.md)

### Under discussion RFCs

* RFC_0007: [Duniter Ascii Armor Messages](https://git.duniter.org/documents/rfcs/blob/ascii_armor_messages/rfc/0007%20Ascii%20Armor%20Messages.md)
* RFC_0008: [Duniter Messages Encryption and Signature](https://git.duniter.org/documents/rfcs/blob/messages_encryption_and_signature/rfc/0008%20Messages%20Encryption%20and%20Signature.md)
* RFC_0013: [Duniter Encrypted Wallet Import Format](https://git.duniter.org/documents/rfcs/blob/dewif/rfc/0013_Duniter_Encrypted_Wallet_Import_Format.md)
* RFC_0014: [DUBP Mnemonic](https://git.duniter.org/documents/rfcs/blob/dubp-mnemonic/rfc/0014_Dubp_Mnemonic.md)

### Drafts RFCs

* RFC_0003: [GraphQL API for Duniter Clients](https://git.duniter.org/documents/rfcs/blob/graphql_api_rfc/rfc/0003%20RFC%20GraphQL%20API%20for%20Duniter%20Clients.md)
* RFC_0006: [DUniter Network Protocol V2](https://git.duniter.org/documents/rfcs/blob/ws2p_v2/rfc/0006_ws2p_v2.md)
* RFC_0011: [Public Key Secure Transport Layer V1](https://git.duniter.org/documents/rfcs/blob/ws2p_v2/rfc/0011_pkstl_v1.md)
* RFC_0011: [Duniter_Blockchain_Protocol_V13](https://git.duniter.org/documents/rfcs/blob/dubp_v13/rfc/0011_Duniter_Blockchain_Protocol_V13.md)
* RFC_0012: [Duniter Key File Format](https://git.duniter.org/documents/rfcs/blob/authentication_file_format/rfc/0012_duniterkey_authentication_file_format_v1.md)
* RFC_0015: [HD wallet](https://git.duniter.org/documents/rfcs/blob/hd_wallet/rfc/0015_Dubp_HD_Wallet.md)

### Discontinued RFCs

* RFC_0001: [Abstract Syntax Tree-based output script locks](https://git.duniter.org/nodes/common/doc/blob/dip/0001/rfc/0001%20Abstract%20Syntax%20Tree-based%20output%20script%20locks.md)
* RFC_0002: [New binary Duniter protocol](https://git.duniter.org/nodes/common/doc/blob/dip/0002/rfc/0002%20New%20binary%20Duniter%20protocol.md)
* RFC_0005: [New Scalable Blockchain Protocol](https://git.duniter.org/nodes/common/doc/blob/rfc5-duniter-protocol-rework/rfc/0005%20New%20Scalable%20Blockchain%20Protocol.md)
