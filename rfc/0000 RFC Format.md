# #0 RFC Format

```txt
RFC: 0
Title: RFC Format
Type: Convention
Status: WIP
Author: nanocryk <nanocryk@gmail.com>
Created: 2017-11-22
Last edited: 2017-12-20
License: AGPL-3
```

A RFC (Request for Changes) is a design document describing new feature for the Duniter ecosystem. It should provide a concise technical specification of the feature and a rationale for the feature.

We intend RFCs to be the primary mechanics for proposing new features, for collecing community input on an issue, and for documenting the design decisions the have gone into Duniter. The RFC author is responsible for building consensus within the community and documenting dissenting opinions.

Because RFCs are maintained as text files in a versioned repository, their revision history is the historical record of the feature proposal.

> TODO : Provide a normalized RFC structure